<div align="center">    
 
# Deep-Inverse  
   

[![Paper](http://img.shields.io/badge/paper-arxiv.1001.2234-B31B1B.svg)](https://papers.nips.cc/paper/2018/file/d903e9608cfbf08910611e4346a0ba44-Paper.pdf)
[![Paper](http://img.shields.io/badge/paper-arxiv.1001.2234-B31B1B.svg)](https://iopscience.iop.org/article/10.1088/1361-6420/aaf14a)
[![Paper](http://img.shields.io/badge/paper-arxiv.1001.2234-B31B1B.svg)](https://arxiv.org/pdf/2008.02839.pdf)
[![Dataset](http://img.shields.io/badge/paper-arxiv.1001.2234-B31B1B.svg)](https://www.nature.com/articles/s41597-021-00893--z)
[![Conference](http://img.shields.io/badge/NeurIPS-2019-4b44ce.svg)](https://papers.nips.cc/book/advances-in-neural-information-processing-systems-31-2018)
[![Conference](http://img.shields.io/badge/ICLR-2019-4b44ce.svg)](https://papers.nips.cc/book/advances-in-neural-information-processing-systems-31-2018)
[![Conference](http://img.shields.io/badge/AnyConference-year-4b44ce.svg)](https://papers.nips.cc/book/advances-in-neural-information-processing-systems-31-2018)  
<!--
ARXIV   
[![Paper](http://img.shields.io/badge/arxiv-math.co:1480.1111-B31B1B.svg)](https://www.nature.com/articles/nature14539)
-->
![CI testing](https://github.com/PyTorchLightning/deep-learning-project-template/workflows/CI%20testing/badge.svg?branch=master&event=push)


<!--  
Conference   
-->   
</div>
 
## GOALS OF THE PROJECT
We would like to build a data-driven model to reconstruct CT images. The model should be evaluated on the LoDoPaB challenge. (https://lodopab.grand-challenge.org/) 

## How to run
We will be using the CUDAs available from the server mathgpu1, provided by Uni-Hamburg. 

# clone project   
git clone https://gitlab.rrz.uni-hamburg.de/BAT9096/deepinverse.git

# requirements
install conda environment with python=3.6, 
install astra-toolbox with stable version, pytorch, and dival.
```
conda install -c astra-toolbox astra-toolbox
conda install pytorch torchvision cudatoolkit=9.0 -c pytorch
conda install dival
```

Make sure to integrate it to the Jupyter kernel. 
   

# run module
We will using the data provided by zonodo.org.  LoDoPaB-CT Dataset.
There will be two data sets, 1. with more than 50GB as zip and the other 3GB as Zip.


## Imports
So far, we are using the following:
pytorch-lightning
torch 
torchvision 
numpy
scipy
matplotlib
scikit-learn
scikit-image
jupyter
imageio



### Citation   
```
@article{YourName,
  title={Your Title},
  author={Your team},
  journal={Location},
  year={Year}
}
```   
