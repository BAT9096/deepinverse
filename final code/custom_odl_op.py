# Copyright 2014-2018 The ODL contributors
#
# This file is part of ODL.
#
# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, You can
# obtain one at https://mozilla.org/MPL/2.0/.

from __future__ import division
from packaging.version import parse as parse_version
import warnings

import numpy as np
import torch

if parse_version(torch.__version__) < parse_version('0.4'):
    warnings.warn("This interface is designed to work with Pytorch >= 0.4",
                  RuntimeWarning)

__all__ = ('OperatorAsAutogradFunction', 'OperatorAsModule')

# TODO: ProductSpaceOperator as implementation of channels_in and channels_out?


class OperatorAsAutogradFunction(torch.autograd.Function):
 #konstruktor weg, static. operator speichern statt im operator in ctx in der forwardmethode, operator wird an fwd übergeben, self wegmachen

    @staticmethod
    def forward(ctx, input, operator):
        ctx.operator = operator
        # TODO: batched evaluation
        if not operator.is_linear:
            # Only needed for nonlinear operators
            ctx.save_for_backward(input)

        # TODO: use GPU memory directly if possible
        input_arr = input.cpu().detach().numpy()
        if any(s == 0 for s in input_arr.strides):
            # TODO: remove when Numpy issue #9165 is fixed
            # https://github.com/numpy/numpy/pull/9177
            input_arr = input_arr.copy()

        op_result = operator(input_arr)
        if np.isscalar(op_result):
            # For functionals, the result is funnelled through `float`,
            # so we wrap it into a Numpy array with the same dtype as
            # `operator.domain`
            op_result = np.array(op_result, ndmin=1,
                                 dtype=operator.domain.dtype)
        tensor = torch.from_numpy(np.array(op_result, copy=False, ndmin=1))
        #if input.is_cuda:
            # Push back to GPU
        tensor = tensor.to(input.device)
        return tensor

    #static, self.op -> ctx.op, self -> ctx
    @staticmethod
    def backward(ctx, grad_output):
        # TODO: implement directly for GPU data
        if not ctx.operator.is_linear:
            input_arr = ctx.saved_variables[0].data.cpu().numpy()
            if any(s == 0 for s in input_arr.strides):
                # TODO: remove when Numpy issue #9165 is fixed
                # https://github.com/numpy/numpy/pull/9177
                input_arr = input_arr.copy()

        grad = None

        # ODL weights spaces, pytorch doesn't, so we need to handle this
        try:
            dom_weight = ctx.operator.domain.weighting.const
        except AttributeError:
            dom_weight = 1.0

        try:
            ran_weight = ctx.operator.range.weighting.const
        except AttributeError:
            ran_weight = 1.0

        scaling = dom_weight / ran_weight

        if ctx.needs_input_grad[0]:
            grad_output_arr = grad_output.cpu().numpy()
            if any(s == 0 for s in grad_output_arr.strides):
                # TODO: remove when Numpy issue #9165 is fixed
                # https://github.com/numpy/numpy/pull/9177
                grad_output_arr = grad_output_arr.copy()

            if ctx.operator.is_linear:
                adjoint = ctx.operator.adjoint
            else:
                adjoint = ctx.operator.derivative(input_arr).adjoint

            grad_odl = adjoint(grad_output_arr)

            if scaling != 1.0:
                grad_odl *= scaling

            grad = torch.from_numpy(np.array(grad_odl, copy=False, ndmin=1))

            if grad_output.is_cuda:
                # Push back to GPU
                grad = grad.cuda()

        return grad, None

    def __repr__(self):
        """Return ``repr(self)``."""
        return '{}(\n    {!r}    \n)'.format(self.__class__.__name__,
                                             self.operator)


class OperatorAsModule(torch.nn.Module):
#self.opfc.op -> self.op, opfc gibts nicht mehr, in 136: opfc(x_flat_extra[i]) -> FKT.apply(operator)
    def __init__(self, operator):
        super(OperatorAsModule, self).__init__()
        self.operator = operator

    def forward(self, x):
        in_shape = x.data.shape
        op_in_shape = self.operator.domain.shape
        op_out_shape = self.operator.range.shape

        extra_shape = in_shape[:-len(op_in_shape)]

        if in_shape[-len(op_in_shape):] != op_in_shape or not extra_shape:
            shp_str = str(op_in_shape).strip('()')
            raise ValueError('expected input of shape (N, *, {}), got input '
                             'with shape {}'.format(shp_str, in_shape))

        # Flatten extra axes, then do one entry at a time
        newshape = (int(np.prod(extra_shape)),) + op_in_shape
        x_flat_xtra = x.reshape(*newshape)
        results = []
        for i in range(x_flat_xtra.data.shape[0]):
            results.append(OperatorAsAutogradFunction.apply(x_flat_xtra[i], self.operator))

        # Reshape the resulting stack to the expected output shape
        stack_flat_xtra = torch.stack(results)
        return stack_flat_xtra.view(extra_shape + op_out_shape)

    def __repr__(self):
        """Return ``repr(self)``."""
        op_name = self.operator.__class__.__name__
        op_dom_shape = self.operator.domain.shape
        if len(op_dom_shape) == 1:
            op_dom_shape = op_dom_shape[0]
        op_ran_shape = self.operator.range.shape
        if len(op_ran_shape) == 1:
            op_ran_shape = op_ran_shape[0]

        return '{}({}) ({} -> {})'.format(self.__class__.__name__,
                                          op_name, op_dom_shape, op_ran_shape)


if __name__ == '__main__':
    from odl.util.testutils import run_doctests
    import odl
    from torch import autograd, nn
    run_doctests(extraglobs={'np': np, 'odl': odl, 'torch': torch,
                             'nn': nn, 'autograd': autograd})