#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='DeepInverse',
    version='0.0.0',
    description='Deep Learning in Inverse Problems',
    author='',
    author_email='',
    # REPLACE WITH YOUR OWN GITHUB PROJECT LINK
    url='https://gitlab.rrz.uni-hamburg.de/BAT9096/deepinverse.git',
    install_requires=['pytorch-lightning'],
    packages=find_packages(),
)

